package goroutinefunctions

import (
	"fmt"
	"os"
	"strconv"
)

func buildString(n int) []string {
	s := []string{}
	for i := 0; i < n; i++ {
		s = append(s, strconv.Itoa(i))
	}

	return s
}

func testfunction(s string, channel chan<- string) {
	/*
		change this function to do the task you need
	*/
	channel <- s
}

func BatchCallGoroutines(currLayer []string) {
	/*
		Batch calls goroutines
	*/
	channel := make(chan string)
	limitgoroutines := 3
	limiteachcycle := 0
	layercount := 0
	dedup := make(map[string]bool)
	layerleft := len(currLayer)

	for layerleft > 0 {
		if layerleft >= limitgoroutines {
			limiteachcycle = limitgoroutines
		} else {
			limiteachcycle = layerleft
		}
		fmt.Printf("calling batch of %d goroutines\n", limiteachcycle)
		for i := 0; i < limiteachcycle; i++ {
			go testfunction(currLayer[layercount], channel)
			dedup[currLayer[layercount]] = true
			layercount++
		}

		for i := 0; i < limiteachcycle; i++ {
			chnOut := <-channel
			fmt.Println(chnOut)
		}

		layerleft -= limiteachcycle
	}
	fmt.Println("layercount", layercount)
}

func main() {
	currLayer := buildString(987777)
	BatchCallGoroutines(currLayer)
}


func WriteToFile(output []byte, filename string) {
	/*
		writes bytes to json file
	*/
	fn := fmt.Sprintf("internal/data/%s.json", filename)
	fo, err := os.Create(fn)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := fo.Close(); err != nil {
			panic(err)
		}
	}()
	if _, err := fo.Write(output); err != nil {
		panic(err)
	}
}

// Unmarshaling arrays items to fields //////

import (
	"encoding/json"
	"fmt"
	"log"
)

const input = `
["Hello world", 10, false]
`

type Notification struct {
	Message  string
	Priority uint8
	Critical bool
}

func (n *Notification) UnmarshalJSON(buf []byte) error {
	tmp := []interface{}{&n.Message, &n.Priority, &n.Critical}
	wantLen := len(tmp)
	if err := json.Unmarshal(buf, &tmp); err != nil {
		return err
	}
	if g, e := len(tmp), wantLen; g != e {
		return fmt.Errorf("wrong number of fields in Notification: %d != %d", g, e)
	}
	return nil
}

func main() {
	var n Notification
	if err := json.Unmarshal([]byte(input), &n); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%#v\n", n)
}

//////////////////////////////////////////////


func (n *Klineentry) UnmarshalJSON(buf []byte) error {
	/*
		overrides the UnmarshalJSON interface to unmarshal json array into struct
		adapted from: https://eagain.net/articles/go-json-array-to-struct/
		https://blog.gopheracademy.com/advent-2016/advanced-encoding-decoding/
		https://medium.com/@dynastymasra/override-json-marshalling-in-go-cb418102c60f
	*/

	// need to populate an interface first
	tmp := []interface{}{
		&n.Opentime,
		&n.Open,
		&n.High,
		&n.Low,
		&n.Close,
		&n.Volume,
		&n.Closetime,
		&n.QuoteAssetVolume,
		&n.NumOfTrades,
		&n.TakerBuyBaseAssetVolume,
		&n.TakerBuyQuoteAssetVolume,
		&n.Ignore,
	}
	wantLen := len(tmp)
	if err := json.Unmarshal(buf, &tmp); err != nil {
		return err
	}
	if g, e := len(tmp), wantLen; g != e {
		return fmt.Errorf("wrong number of fields: %d != %d", g, e)
	}
	return nil
}